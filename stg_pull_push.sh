aws ecr get-login-password | docker login --username AWS --password-stdin 695200269817.dkr.ecr.eu-west-1.amazonaws.com/stgpullpush
docker pull enriqueramosmunoz/auth:$1
docker tag enriqueramosmunoz/auth:$1 695200269817.dkr.ecr.eu-west-1.amazonaws.com/stgpullpush:$1
docker push 695200269817.dkr.ecr.eu-west-1.amazonaws.com/stgpullpush:$1